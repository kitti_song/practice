import React, { Component } from 'react';
import { Input, Label, Button, Row, Col, Card } from 'reactstrap'

import '../App.css'

let openBracket = 0
let closeBracket = 0
class CheckBracket extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: '',
      result: ''
    }
  }

  componentWillMount() {
    openBracket = 0
    closeBracket = 0
    this.setState({ data: '' })
  }
  componentWillReceiveProps(nextprops) {

  }

  checkBracket() {
    const { data } = this.state
    openBracket = 0
    closeBracket = 0
    for (let i = 0; i <= data.length; i++) {

      if (data.charAt(i) == '(') {
        openBracket++
      }
      else if (data.charAt(i) == ')') {
        closeBracket++
      }
    }
    this.display()
  }
  display() {
    console.log(openBracket)
    console.log(closeBracket)
    if (openBracket == 0 && closeBracket == 0) {
      this.setState({ result: 'ไม่มีวงเล็บ' })
    }
    else if (openBracket != closeBracket) {

      this.setState({ result: 'invalid' })
    }
    else {

      this.setState({ result: 'valid' })
    }
  }
  render() {
    return (
      <div className="App">

        < Card style={{ marginTop: 50, padding: 10, display: 'inline-flex', maxWidth: 500 }}>
          <Row style={{ paddingBottom: 10, paddingTop: 20 }}>
            <Col md={12}>
              <Label style={{ paddingRight: 10 }}>Input</Label>
              <Input type='text' onChange={(event) => this.setState({ data: event.target.value })} />
            </Col>

          </Row>
          <Row style={{ justifyContent: 'center' }}>
            <Button color='primary' onClick={() => this.checkBracket()}>ตรวจสอบ</Button>
          </Row>

        </Card >
        <div>
          <div>
            {this.state.result}
          </div>
        </div>

      </div>
    );
  }
}

export default CheckBracket;
