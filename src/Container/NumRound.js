import React, { Component } from 'react'
import { Input, Label, Button, Row, Col, Card } from 'reactstrap'

let result = 0
class NumRound extends Component {
    constructor(props) {
        super(props)
        this.state = {
            input: ''
        }
    }

    process() {
        result = 0
        let sub = 0
        let frontsub = 0
        // let check
        const { input } = this.state
        for (let i = 0; i < input.length; ++i) {
            if (input.charAt(i) == '.') {
                frontsub = input.substring(0, i)
            }
        }
        /*------------------------------------*/
        console.log(frontsub)
        sub = input.substring(input.length - 2)
        if (sub.charAt(0) == '.') {
            sub = (sub.substring(sub.length - 1)) + '0'

        }
        /*------------------------------------*/
        if (sub >= 0 && sub < 25) {
            if (sub - 0 < 25 - sub) {
                sub = 0
                result = parseFloat(frontsub + '.' + sub).toFixed(2)
            }
            else {
                sub = 25
                result = frontsub + '.' + sub
            }
        }
        else if (sub >= 25 && sub < 50) {
            if (sub - 25 < 50 - sub) {
                sub = 25
                result = frontsub + '.' + sub
            }
            else {
                sub = 50
                result = frontsub + '.' + sub
            }
        }
        else if (sub >= 50 && sub < 75) {
            if (sub - 50 < 75 - sub) {
                sub = 50
                result = frontsub + '.' + sub
            }
            else {
                sub = 75
                result = frontsub + '.' + sub
            }
        }
        else if (sub >= 75 && sub < 100) {
            if (sub - 75 < 100 - sub) {
                sub = 75
                result = frontsub + '.' + sub
            }
            else {
                sub = 0
                result = (parseFloat(frontsub + '.' + sub) + 1.00).toFixed(2)
            }
        }
        alert(result)
        //console.log(result)
    }

    render() {

        return (
            <div className="App">
                < Card style={{ marginTop: 50, padding: 10, display: 'inline-flex', maxWidth: 500 }}>
                    <Row style={{ paddingBottom: 10, paddingTop: 20 }}>
                        <Col md={12}>
                            <Label>Input</Label>
                            <Input type='text' onChange={(event) => this.setState({ input: event.target.value })} />
                        </Col>

                    </Row>


                    <Row style={{ justifyContent: 'center' }}>
                        <Button color='primary' onClick={() => this.process()}>ปัดเศษ</Button>
                    </Row>
                </Card>

            </div>
        )
    }
}
export default NumRound