import React, { Component } from 'react';
//import logo from './logo.svg';
import { BrowserRouter as Router, Route, NavLink } from 'react-router-dom';
//import './App.css';
import CheckBracket from './Container/CheckBracket';
import SortArray from './Container/SortArray';
import NumRound from './Container/NumRound'
import FindCharacter from './Container/FindCharacter'
import 'bootstrap/dist/css/bootstrap.css';
import { Row, Col } from 'reactstrap'

class App extends Component {

  render() {
    return (

      <Router>
        <div>
          < ul style={{ paddingTop: 15, paddingBottom: 15, margin: 0, backgroundColor: 'black' }}>
            <Row>
              {/* <p style={{ margin: 0 }}> */}
              <Col md={3} xs={6}>
                <NavLink style={{ textDecoration: 'none' }} to="/SortArray" exact activeStyle={{ color: 'orange' }}>เรียงลำดับ Array</NavLink>
              </Col>
              <Col md={3} xs={6}>
                <NavLink style={{ textDecoration: 'none' }} to="/NumRound" exact activeStyle={{ color: 'orange' }}>ปัดเศษ</NavLink>
              </Col>
              <Col md={3} xs={6}>
                <NavLink style={{ textDecoration: 'none' }} to="/CheckBracket" exact activeStyle={{ color: 'orange' }}>ตรวจวงเล็บ</NavLink>
              </Col>
              <Col md={3} xs={6}>
                <NavLink style={{ textDecoration: 'none' }} to="/FindCharacter" exact activeStyle={{ color: 'orange' }}>หาอักษรในวงเล็บ</NavLink>
              </Col>
              {/* </p> */}
            </Row>

          </ul>
          <Route path="/FindCharacter" exact render={
            () => {
              return <FindCharacter />
            }
          } />
          <Route path="/SortArray" exact render={
            () => {
              return <SortArray />
            }
          } />
          <Route path="/NumRound" exact render={
            () => {
              return <NumRound />
            }
          } />
          <Route path="/CheckBracket" exact render={
            () => {
              return <CheckBracket />;
            }
          } />


        </div>
      </Router >
    );
  }
}

export default App;
